FactoryGirl.define do
  factory :generic_payment, class: :payment do
    association :loan, factory: :generic_loan
    credited_amount 50.0
  end
end
