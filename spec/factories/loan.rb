FactoryGirl.define do
  factory :generic_loan, class: :loan do
    funded_amount 100.0
  end
end
