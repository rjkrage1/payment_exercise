require 'rails_helper'

RSpec.describe LoansController, type: :controller do
  describe '#index' do

    it 'responds with a 200' do
      get :index
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to be_a Array
    end
  end

  describe '#show' do

    it 'responds with a 200 with correct amount' do
      loan = create(:generic_loan)
      get :show, id: loan.id
      expect(response).to have_http_status(:ok)
      funded_amount = JSON.parse(response.body)['funded_amount'].to_d
      expect(loan.funded_amount).to eq funded_amount
    end

    context 'if the loan is not found' do
      it 'responds with a 404' do
        get :show, id: -1
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe '#create' do

    it 'responds with a 201 with correct amount' do
      post :create, attributes_for(:generic_loan)
      expect(response).to have_http_status(:created)
      response_body = JSON.parse(response.body)
      loan = Loan.find response_body['id']
      expect(loan.balance).to eq response_body['balance'].to_d
      expect(loan.balance).to eq response_body['funded_amount'].to_d
      expect(loan.funded_amount).to eq response_body['funded_amount'].to_d
    end

    it 'responds with 422 and error messages when missing params' do
      # try to create loan without funded_amount
      loan = attributes_for(:generic_loan)
      loan.delete :funded_amount
      post :create, loan
      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)['messages']).to be_a Array
    end
  end
end
