require 'rails_helper'

RSpec.describe PaymentsController, type: :controller do
  describe '#index' do

    before :all do
      # create a bunch of loans and payments
      10.times { create(:generic_payment) }
    end

    it 'responds with a 200' do
      get :index
      expect(response).to have_http_status(:ok)
      response_body = JSON.parse(response.body)
      expect(response_body).to be_a Array
      expect(response_body.size).to be > 0
    end

    it 'responds with a 200 with payments for a specific loan' do
      payment = create(:generic_payment)

      get :index, loan_id: payment.loan_id
      expect(response).to have_http_status(:ok)
      response_body = JSON.parse(response.body)
      expect(response_body).to be_a Array
      expect(response_body.size).to eq 1
      expect(response_body.first['loan_id']).to eq payment.loan_id
    end
  end

  describe '#show' do

    it 'responds with a 200 with correct amount' do
      payment = create(:generic_payment)
      get :show, id: payment.id
      expect(response).to have_http_status(:ok)
      payment_amount = JSON.parse(response.body)['credited_amount'].to_d
      expect(payment.credited_amount).to eq payment_amount
    end

    context 'if the loan is not found' do
      it 'responds with a 404' do
        get :show, id: -1
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe '#create' do

    it 'responds with a 201 and reduces balance' do
      loan = create(:generic_loan)
      initial_balance = loan.balance
      post :create, attributes_for(:generic_payment, loan_id: loan.id)
      expect(response).to have_http_status(:created)
      payment_amount = JSON.parse(response.body)['credited_amount'].to_d
      expect(loan.reload.balance).to eq(initial_balance - payment_amount)
    end

    it 'responds with 422 and error messages when balance goes below 0' do
      loan = create(:generic_loan)
      payment = attributes_for(:generic_payment)
      payment[:credited_amount] = 50.0 + loan.funded_amount
      payment[:loan_id] = loan.id
      post :create, payment
      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)['messages']).to be_a Array
    end

    it 'responds with 422 and error messages when missing params' do
      # try to create payment without loan_id
      post :create, attributes_for(:generic_payment)
      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)['messages']).to be_a Array
    end
  end
end
