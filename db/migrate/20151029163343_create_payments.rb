class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.decimal :credited_amount, precision: 8, scale: 2, null: false
      t.belongs_to :loan, index: true, null: false
      t.timestamps null: false
    end
  end
end
