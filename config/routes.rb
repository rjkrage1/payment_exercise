Rails.application.routes.draw do

  resources :loans, defaults: {format: :json}, only: [:index, :show, :create] do
    resources :payments, only: :index
  end

  resources :payments, defaults: {format: :json}, only: [:index, :show, :create]
end
