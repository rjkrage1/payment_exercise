class PaymentSerializer < ActiveModel::Serializer
  attributes :id, :loan_id, :created_at, :credited_amount
end
