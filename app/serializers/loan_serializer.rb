class LoanSerializer < ActiveModel::Serializer
  attributes :id, :created_at, :funded_amount, :balance
end
