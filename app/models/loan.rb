class Loan < ActiveRecord::Base

  has_many :payments, dependent: :destroy

  validates :funded_amount, presence: true

  validates :funded_amount, numericality: {greater_than: 0}

  validates :balance, numericality: {greater_than_or_equal_to: 0}

  # initially default balance to funded amount
  before_validation do |record|
    record.balance = record.funded_amount unless record.balance
  end

  def update_balance!
    computed_balance = funded_amount
    payments.each { |payment| computed_balance -= payment.credited_amount }

    update_attributes! balance: computed_balance
  end
end
