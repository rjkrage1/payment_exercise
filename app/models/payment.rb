class Payment < ActiveRecord::Base

  belongs_to :loan

  validates :loan, :credited_amount, presence: true

  validates :credited_amount, numericality: {greater_than: 0}

  def save_and_update_balance
    transaction do
      save! && loan.update_balance!
    end
  rescue ActiveRecord::RecordInvalid
    # copy loan error messages to payment object
    if loan
      loan.errors.each do |attribute, message|
        errors.add attribute, message
      end
    end
    false
  end

end
