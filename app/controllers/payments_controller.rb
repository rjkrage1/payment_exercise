class PaymentsController < ApplicationController

  def index
    if params[:loan_id]
      render json: Loan.find(params[:loan_id]).payments
    else
      render json: Payment.all
    end
  end

  def show
    render json: Payment.find(params[:id])
  end

  def create
    @model = Payment.new(payment_params)

    if @model.save_and_update_balance
      render json: @model, status: :created, location: @model
    else
      show_model_errors
    end
  end

  private

  # params allowed in POST body
  def payment_params
    params.permit(:credited_amount, :loan_id)
  end
end
