class LoansController < ApplicationController

  def index
    render json: Loan.all
  end

  def show
    render json: Loan.find(params[:id])
  end

  def create
    @model = Loan.new(loan_params)

    if @model.save
      render json: @model, status: :created, location: @model
    else
      show_model_errors
    end
  end

  private

  # params allowed in POST body
  def loan_params
    params.permit(:funded_amount)
  end
end
