class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  rescue_from ActiveRecord::RecordNotFound do
    render json: 'not_found', status: :not_found
  end

  private

  # generic function to render model validation errors
  def show_model_errors
    render json: {messages: @model.errors.full_messages}, status: :unprocessable_entity
  end
end
